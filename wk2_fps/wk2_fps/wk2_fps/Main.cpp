#include <GL/freeglut.h>
#include <cstdio>
#define _USE_MATH_DEFINES
#include <cmath>

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"

#define STB_PERLIN_IMPLEMENTATION
#include "stb_perlin.h"

#include <iostream>
#include <string>
using namespace std;
#include <time.h>

float lastFrameTime = 0;

int width, height;
GLuint grass;
GLuint sheet;

int map[10][32][32];

struct Camera
{
	float posX = 0;
	float posY = -4;
	float posZ = -2;
	float rotX = 0;
	float rotY = 0;
} camera;

bool keys[255];

void drawSheetCube(int index)
{
	if (index == -1)
		return;

	float x = (index % 16);
	float y = (index / 16)*(1/16.0f);
	float xend = ((float)1 / 16);
	float yend = ((float)1 / 16);
	//cout << row << endl;
	//cout << "----" << endl;
	//cout << column << endl;


	glBindTexture(GL_TEXTURE_2D, sheet);
	glBegin(GL_QUADS);
	glColor3f(1.0f, 1.0f, 1.0f);
	//glColor3f(1, 0, 0);
	glTexCoord2f(x,y); glVertex3f(-0.5, -0.5, -0.5);
	glTexCoord2f(x, y+yend); glVertex3f(0.5, -0.5, -0.5);
	glTexCoord2f(x+xend, y+yend); glVertex3f(0.5, 0.5, -0.5);
	glTexCoord2f(x+ xend, y); glVertex3f(-0.5, 0.5, -0.5);

	//glColor3f(0.5, 0.5, 0);
	glTexCoord2f(x,y); glVertex3f(-0.5, -0.5, 0.5);
	glTexCoord2f(x, y+yend); glVertex3f(0.5, -0.5, 0.5);
	glTexCoord2f(x+xend, y+yend); glVertex3f(0.5, 0.5, 0.5);
	glTexCoord2f(x+xend, y); glVertex3f(-0.5, 0.5, 0.5);

	//glColor3f(0, 0, 0.5);
	glTexCoord2f(x, y); glVertex3f(-0.5, -0.5, -0.5);
	glTexCoord2f(x, y+yend); glVertex3f(-0.5, 0.5, -0.5);
	glTexCoord2f(x+xend, y+yend); glVertex3f(-0.5, 0.5, 0.5);
	glTexCoord2f(x+xend, y); glVertex3f(-0.5, -0.5, 0.5);

	//glColor3f(0.5, -0.5, 0.5);
	glTexCoord2f(x, y); glVertex3f(0.5, -0.5, -0.5);
	glTexCoord2f(x, y+yend); glVertex3f(0.5, 0.5, -0.5);
	glTexCoord2f(x+xend, y+yend); glVertex3f(0.5, 0.5, 0.5);
	glTexCoord2f(x+xend, y); glVertex3f(0.5, -0.5, 0.5);

	//glColor3f(0, 0.5, 0);
	glTexCoord2f(x, y); glVertex3f(-0.5, -0.5, -0.5);
	glTexCoord2f(x, y+yend); glVertex3f(0.5, -0.5, -0.5);
	glTexCoord2f(x+xend, y+yend); glVertex3f(0.5, -0.5, 0.5);
	glTexCoord2f(x+xend, y); glVertex3f(-0.5, -0.5, 0.5);

	//glColor3f(0.5, 0.5, 0);
	glTexCoord2f(x, y); glVertex3f(-0.5, 0.5, -0.5);
	glTexCoord2f(x, y+yend); glVertex3f(0.5, 0.5, -0.5);
	glTexCoord2f(x+xend, y+yend); glVertex3f(0.5, 0.5, 0.5);
	glTexCoord2f(x+xend, y); glVertex3f(-0.5, 0.5, 0.5);
	glEnd();
}


void drawCube()
{
	glBindTexture(GL_TEXTURE_2D, grass);
	glBegin(GL_QUADS);
	//glColor3f(1, 0, 0);
	glTexCoord2f(0, 0); glVertex3f(-1, -1, -1);
	glTexCoord2f(0, 1); glVertex3f(1, -1, -1);
	glTexCoord2f(1, 1); glVertex3f(1, 1, -1);
	glTexCoord2f(1, 0); glVertex3f(-1, 1, -1);

	//glColor3f(1, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-1, -1, 1);
	glTexCoord2f(0, 1); glVertex3f(1, -1, 1);
	glTexCoord2f(1, 1); glVertex3f(1, 1, 1);
	glTexCoord2f(1, 0); glVertex3f(-1, 1, 1);

	//glColor3f(0, 0, 1);
	glTexCoord2f(0, 0); glVertex3f(-1, -1, -1);
	glTexCoord2f(0, 1); glVertex3f(-1, 1, -1);
	glTexCoord2f(1, 1); glVertex3f(-1, 1, 1);
	glTexCoord2f(1, 0); glVertex3f(-1, -1, 1);

	//glColor3f(1, -1, 1);
	glTexCoord2f(0, 0); glVertex3f(1, -1, -1);
	glTexCoord2f(0, 1); glVertex3f(1, 1, -1);
	glTexCoord2f(1, 1); glVertex3f(1, 1, 1);
	glTexCoord2f(1, 0); glVertex3f(1, -1, 1);

	//glColor3f(0, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-1, -1, -1);
	glTexCoord2f(0, 1); glVertex3f(1, -1, -1);
	glTexCoord2f(1, 1); glVertex3f(1, -1, 1);
	glTexCoord2f(1, 0); glVertex3f(-1, -1, 1);

	//glColor3f(1, 1, 0);
	glTexCoord2f(0, 0); glVertex3f(-1, 1, -1);
	glTexCoord2f(0, 1); glVertex3f(1, 1, -1);
	glTexCoord2f(1, 1); glVertex3f(1, 1, 1);
	glTexCoord2f(1, 0); glVertex3f(-1, 1, 1);
	glEnd();
}


void display()
{
	glClearColor(0.6f, 0.6f, 1, 1);
	glClear(GL_DEPTH_BUFFER_BIT | GL_COLOR_BUFFER_BIT);

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(60.0f, (float)width/height, 0.1, 30);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glRotatef(camera.rotX, 1, 0, 0);
	glRotatef(camera.rotY, 0, 1, 0);
	glTranslatef(camera.posX, camera.posZ, camera.posY);

	glPushMatrix();

	glBegin(GL_LINES);
	glColor3f(1, 0, 0); // X axis is red.
	glVertex3f(0, 0, 0);
	glVertex3f(50, 0, 0);
	glColor3f(0, 1, 0); // Y axis is green.
	glVertex3f(0, 0, 0);
	glVertex3f(0, 50, 0);
	glColor3f(0, 0, 1); // z axis is blue.
	glVertex3f(0, 0, 0);
	glVertex3f(0, 0, 50);
	glEnd();

	glPopMatrix();

	glPushMatrix();
	glTranslatef(2, 0, 0);
	drawSheetCube(16);
	
	glPopMatrix();


	/*glColor3f(0.1f, 1.0f, 0.2f);
	glBindTexture(GL_TEXTURE_2D, grass);
	glBegin(GL_QUADS);
	glTexCoord2f(0, 0); glVertex3f(-15, -1, -15);
	glTexCoord2f(0, 1); glVertex3f( 15, -1, -15);
	glTexCoord2f(1, 1); glVertex3f( 15, -1,  15);
	glTexCoord2f(1, 0); glVertex3f(-15, -1,  15);
	glEnd();

	for (int x = -10; x <= 10; x += 5)
	{
		for (int y = -10; y <= 10; y += 5)
		{
			glPushMatrix();
			glTranslatef((float)x, 0.0f, (float)y);
			drawCube();
			glPopMatrix();
		}
	}

	glPushMatrix();
	glTranslatef(0, 10.0f, 0);
	drawSheetCube(16);
	glPopMatrix();*/

	
	//glTranslatef(15, 15, 15);
	for (int y = 0; y < 10; y++)
	{
		for (int z = 0; z < 32; z++)
		{
			for (int x = 0; x < 32; x++)
			{

				glPushMatrix();
				glTranslatef(x*1.0f, y*1.0f, z*1.0f);
				drawSheetCube(map[y][z][x]);
				glPopMatrix();

			}
		}

	}
	
	//glFogi(GL_FOG_MODE, GL_LINEAR);        // Fog Mode
	//GLfloat fogColor[4] = { 0.5f,0.5f,0.5f,1.0f };
	//glFogfv(GL_FOG_COLOR, fogColor);            // Set Fog Color
	//glFogf(GL_FOG_DENSITY, 0.35f);              // How Dense Will The Fog Be
	//glHint(GL_FOG_HINT, GL_DONT_CARE);          // Fog Hint Value
	//glFogf(GL_FOG_START, 1.0f);             // Fog Start Depth
	//glFogf(GL_FOG_END, 7.0f);               // Fog End Depth
	//glEnable(GL_FOG);                   // Enables GL_FOG

	glutSwapBuffers();
}

void move(float angle, float fac)
{
	camera.posX += (float)cos((camera.rotY + angle) / 180 * M_PI) * fac;
	camera.posY += (float)sin((camera.rotY + angle) / 180 * M_PI) * fac;
}

void idle()
{
	float frameTime = glutGet(GLUT_ELAPSED_TIME)/1000.0f;
	float deltaTime = frameTime - lastFrameTime;
	lastFrameTime = frameTime;

	const float speed = 3;
	if (keys['a']) move(0, deltaTime*speed);
	if (keys['d']) move(180, deltaTime*speed);
	if (keys['w']) move(90, deltaTime*speed);
	if (keys['s']) move(270, deltaTime*speed);
	if (keys['q']) camera.posZ += deltaTime*speed;
	if (keys['e']) camera.posZ -= deltaTime*speed;

	//cout << camera.posX;
	//cout << "-";
	//cout << camera.posY << endl;

	//cout << frameTime << endl;


	glutPostRedisplay();
}

void mousePassiveMotion(int x, int y)
{
	int dx = x - width / 2;
	int dy = y - height / 2;
	if ((dx != 0 || dy != 0) && abs(dx) < 400 && abs(dy) < 400)
	{
		camera.rotY += dx / 10.0f;
		camera.rotX += dy / 10.0f;
		glutWarpPointer(width / 2, height / 2);
	}
}

void keyboard(unsigned char key, int, int)
{
	if (key == 27)
		exit(0);
	keys[key] = true;
}

void keyboardUp(unsigned char key, int,int)
{
	keys[key] = false;
}

int main(int argc, char* argv[])
{
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE | GLUT_DEPTH);
	glutInitWindowSize(800, 600);
	glutInit(&argc, argv);
	glutCreateWindow("Hello World");

	memset(keys, 0, sizeof(keys));
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &grass);
	glBindTexture(GL_TEXTURE_2D, grass);

	int imgwidth, imgheight, bpp;
	char* imgData = (char*) stbi_load("grass_grass_0099_02_preview.jpg", &imgwidth, &imgheight, &bpp, 4);
	glTexImage2D(GL_TEXTURE_2D,0,GL_RGBA,imgwidth,imgheight,0,GL_RGBA,GL_UNSIGNED_BYTE,(unsigned char*)imgData);
	stbi_image_free(imgData);

	/*unsigned char data[32 * 32 * 4];
	for (int i = 0; i < 32 * 32 * 4; i++)
		data[i] = rand() % 256;*/

	//glTexImage2D(GL_TEXTURE_2D,
	//	0,		//level
	//	GL_RGBA,		//internal format
	//	32,		//width
	//	32,		//height
	//	0,		//border
	//	GL_RGBA,		//data format
	//	GL_UNSIGNED_BYTE,	//data type
	//	data);		//data
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

	glGenTextures(1, &sheet);
	glBindTexture(GL_TEXTURE_2D, sheet);

	int sheetwidth, sheetheight, sheetbpp;
	char* sheetData= (char*)stbi_load("terrain2.png", &sheetwidth, &sheetheight, &sheetbpp, 4);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, sheetwidth, sheetheight, 0, GL_RGBA, GL_UNSIGNED_BYTE, (unsigned char*)sheetData);
	stbi_image_free(sheetData);

	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	/*for (int y = 0; y < 10; y++)
	{
		for (int z = 0; z < 32; z++)
		{
			for (int x = 0; x < 32; x++)
			{
				if(y==9)
				map[y][z][x]=2;
				else { map[y][z][x] = 16; }
			}
		}
	}*/
	std::srand(time(NULL));
	float rand = std::rand() * (1.0f / 10);


	for (int x = 0; x < 32; x++)
	{
		for (int z = 0; z < 32; z++)
		{
			float h = stb_perlin_noise3((1.0f / (float)32)*(float)x, (1.0f / (float)32)*(float)z, rand);
			int heightI = abs((int)(h * 8));

			for (int y = 0; y < 10; y++)
			{
				if (y < 10 / 2)
					map[y][z][x] = 16;
				else if (y < (heightI + 10 / 2))
					map[y][z][x] = 16;
				else if (y == (heightI + 10 / 2))
				{
					map[y][z][x] = 2;
				}
				else
					map[y][z][x] = -1;
			}
		}
	}


	glutIdleFunc(idle);
	glutDisplayFunc(display);
	glutReshapeFunc([](int w, int h) { width = w; height = h; glViewport(0, 0, w, h); });
	glutKeyboardFunc(keyboard);
	glutKeyboardUpFunc(keyboardUp);
	glutPassiveMotionFunc(mousePassiveMotion);

	glutWarpPointer(width / 2, height / 2);


	

	glutMainLoop();


	return 0;
}